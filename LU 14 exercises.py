#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 12 12:48:49 2018

@author: zofia
"""

# class example 1
# what happens when you run this code?

def anarchy():     
    print('anarchy')

some_func = anarchy

some_func()

# when you call the call the variable you get the result anarchy
# actually calling th evariable works here !



# class example 2

def rainbows_and_puppies():    
    print('anarchy')

rainbows_and_puppies()
# ...looks like the same function like b4



# class example 3

def anarchy():
    print('anarchy')

rainbows_and_puppies = anarchy

anarchy()
rainbows_and_puppies()

# class example 4

def func_caller(func):     
    func()

def law_and_order():      
    print('law and order')

func_caller(law_and_order)

# EXERCISES !!!!!!!!!

# exercise 1

def function1(func):    
    func()
    return None

# worked. It called, returned value of the called function
# and didnt return anything as function1 return


# exercise 2
# Write a function that
# Takes two functions as it’s arguments
# Calls both functions
# Returns None

def silent_caller(func1, func2):     
    func1()
    func2()
    return None
# Outcome: it called both functions and returned none.
# Now how I checked that - I wrote other function:

def silent_caller(func1, func2):     
    func1()
    func2()
    return 3
# and here it called both functions, + it returned 3 as "Out[60]: 3"

# It called both functions

# exercise 3
# Write a function that: 
# Takes an integer as it’s first argument
# Takes a function as it’s second argument,
# Calls the function if the first argument is > 10

def conditional_caller(number, func):
    if number > 10 :
        func()
        
conditional_caller(11, law_and_order)
conditional_caller(9, law_and_order)
# WORKS :))))))


# exercise 4
# Write a function that: 
# Takes a two functions as arguments
# Calls the second function if the first function returns true
# Returns None

def doesnt_make_sense(func1, func2):      
    if func1 == True:
        func2
        return None 

def my_birth_year(current_year, number):
    if current_year - number == 1995 :
        return True

def youre_young():      
    print('youre young dude')
    
current_year = 2018
number = 23 

doesnt_make_sense(my_birth_year, youre_young)

# question - did it work haha ? it didnt return anything but ......
# I think it worked. Just not sure how the effect of "return None" should look like


# exercise 5 - ASK IF CORRECT ?
# Write a function that: 
# Takes one function as an argument
# Returns the return value of the argument

def func_caller_2(func):   
    return func

func_caller_2(youre_young())
# works

func_caller_2(my_birth_year(2018, 23)) 
# ok, HERE GO BACK TO KWARGS TO SEE WHY you have problems when you want to just go with delauft options
# however -  works

# exercise 6 
# Write a function that:
# Takes a two functions as arguments
# Returns the return value of the second argument if the return value of the first 
# argument is True

def lol_function(func1, func2):    
    if func1 == True:    
        return func2()
    
def italian_nationality(nationality):
    if nationality == 'italian':     
        return True
    else:      
        return False

def order_pizza():     
    print('order pizza')
    
# PROBABLY RETURNS THE VALUE BUT I DONT SEE IT SO IDK HOW TO CHECK IT 100%













