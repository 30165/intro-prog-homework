# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# LU 5 - FLOW CONTROL & IF, OR, etc ....

# EX 1 
# What is the boolean value of the following
# The number “1”
# The float “1.1”
# The number “0”
# The float “0.0001”
# The empty string “”
# The null value “None”

bool(1)
# Out[1]: True
bool(1.1)
# Out[2]: True
bool(0)
# Out[3]: False
bool(0.0001)
# Out[5]: True
bool()
# Out[6]: False
bool(" ")
# Out[7]: True
bool(None)
# Out[8]: False

# EX 2 
# Write a function:
# That takes a single argument which is a ticker symbol
# Returns True if the ticker symbol is “APPL”
# Returns False otherwise

def is_it_APPL (ticker_symbol):    
    if ticker_symbol == 'APPL':
        return True
    else:
        return False
    
ticker_symbol = 'APPL'

is_it_APPL(ticker_symbol)
is_it_APPL('APPL')
#true
ticker_symbol = 'GOOG'
is_it_APPL(ticker_symbol)
#false


# EX 3
# Write a function
# That takes a single argument which is your gender
# Returns “You like pink” if the gender is ‘male’ and “You hate pink” if the
# gender is ‘female’

def gender_preferences(gender):    
    if gender == 'male':    
        return 'You hate pink'
    if gender == 'female':    
        return 'You like pink'

#now works


# EX 4

def gender_preferences(gender):    
    if gender == 'male':    
        print('You hate pink')
    if gender == 'female':    
        print('You like pink')
    print('this line shouldn\'t be printed')

# observations:
# - like that, 'this line shouldn\'t be printed' is printed
# - if we delete that line, the function doesnt return anything
# - the line is not connected with the function, it will be printed anyway
# but still i think it's not the right solution


# EX 5 

def parents_age(mom_age, dad_age):    
    if mom_age == dad_age:    
        return mom_age + dad_age
    else:
        return dad_age - mom_age   
    
mom_age = 49
dad_age = 51

# EX 6 
# Write a function that takes a dictionary and a list as an argument. 
# If the dictionary has as many items as the size of the list, 
# return “awesome”. If they have different values return “that is sad”.

def dict_vs_list (a_dict, a_list):    
    if len(a_dict) == len(a_list):    
        return 'awesome'     
    else:    
        return'that is sad'

#works

# EX 7

stock_dict = {'APPL': 354, 'GOOG': 234, 'FB': 451}

# Write 3 different loops that loop over this dictionary that
# Print the keys one at a time
# Print the values one at a time
# Print a string that prints “the stock price of <company> is <value>” where <company> and
# <value> are the company stock symbols and stock prices respectively
 
for key in stock_dict:
    print(stock_dict['APPL'])

# prints 3 times 354. Why ? Why it prints it as many times as there are variables?

for key in stock_dict:
    print('APPL', key, 'value', stock_dict[key])

for key in stock_dict:
    print('key', key, 'value', stock_dict[key])
#IT'S CORRRECT NOW
for key in stock_dict:
    print('APPL', key, 'value', stock_dict['key'])
#KeyError

for key in stock_dict:
    print('APPL','value', stock_dict['APPL'])
    
#APPL value 354
#APPL value 354
#APPL value 354

for key in stock_dict:
    print('the stock price of','APPL','is',stock_dict['APPL'],'where', 'APPL', 'and', stock_dict['APPL'], 'are the company stock symbols and stock prices respectively' )

# WORKS

# EX 8

a_tuple = (10, 20, 30)
a_list = (40, 50, 60)
a_dict = {'APPL': 100, 'GOOG': 90, 'FB': 80}

for num in a_tuple:    
    print(num)
# NOW CORRECT 

# EX 9 

# POPRAW !!!!!!
def list_multip (list):    
    result = [i * 2 for i in list]
    return result
#list_multip(list)
#Out[76]: [80, 100, 120]

# CORRECT ONE IN A DAY FINALLY WTF
    
# EX 10

def dict_multip(a_dict):
    new_dict = { }   
    for key in a_dict:
        new_dict[key] = 2*a_dict[key]

    return new_dict
    

# SHORTCUTS:
# highlight formula + click ctrl/enter - it executes that one formula
# in REPL hit upper arrow - it will show you things from REPL history


# jak nie ma int_py ot jest zwykly folder directory
#jak to jest, tojest to package, który można przywoływać








